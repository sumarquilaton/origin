<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'origin_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '3Ng,biF~7F(<J6D&]O5HjrB]u=[XmW}q6@mKC0dXHz;YbE9<DUyZ0a~ v2I}YLZw');
define('SECURE_AUTH_KEY',  '`Nasxe]e91NA<Enq~zj!(c)S!rPy-[84T5RX9NMcTX%{7<bQeh<o/myO5@}FI3d&');
define('LOGGED_IN_KEY',    ')VKy.m=pl}C?UZt[r7$2>gi:>Gdm={ 2H%N>a^GH4ltkbe89)+omq##sqR4=;4MI');
define('NONCE_KEY',        'bhbWK_)*)VEIMBL^SQh~UU(=lqzVg5L4V?_!SSF4zQZnKkk-X;%UP=mkY7j[yhn=');
define('AUTH_SALT',        'CD97[?dFg,Jwyg0ug}F+?Jp)jl|bTqTap{jz72^=Q:v!Mb86)KkCEY6$Ae5N2.75');
define('SECURE_AUTH_SALT', 'U8f3sf6|`q8W;NFe.+FTARtyO^8:JU%!iVQ3z3S6^<_%6!B>i$*m6wkbLjfQcxs7');
define('LOGGED_IN_SALT',   ':. [&dle%s`P}A<*Y>On5VEBlRx(YgcO8P>^$<B7I;|zwAYzbvWX0{Pc41gR~^;x');
define('NONCE_SALT',       'fQUT^q>U;)UE=rAhmqi,dp8uk[-l]l![fcy?fwtk=UmY3nysAtpgX{_FO`2~3R4,');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'or_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
