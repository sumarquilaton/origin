<?php
/**
 * Template Name: Testing and Training
 *
 *
 */

get_header();

global $post;
$post_slug=$post->post_name;

$bg_img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );

if ( have_posts() ) : while ( have_posts() ) : the_post();
?>
    <div class="section-banner animatedParent animateOnce">
        <div class="banner-home bg-inline" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/banner-learning.png');">
            <div class="time-banner">
                <?php //include_once "inc/time.php"?>
            </div>
            <div class="container-fluid">
                <div class="banner-content transform-y">
                    <h2 class="animated fadeInUpShort go">E-Learning</h2>
                    <p class="animated fadeInUpShort delay-250 go">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <div class="gap-30"></div>
                    <div class="animated fadeInUpShort delay-250 go">
                        <a href="<?php the_field('button_link'); ?>" class="btn-common">Watch Video</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-title text-center">
                <h3 class="animated fadeInUpShort">Features</h3>
                <p class="animated fadeInUpShort delay-250">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt </p>
            </div>
            <div class="section-cols features-cols section-top afterclear animated fadeInUpShort">
                <div class="col-md-4 p-0">
                    <div class="col-content">
                        <div class="feature-icon">
                            <div class="feat-img transform-50 afterclear text-center">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-f1.png" alt="">
                            </div>
                        </div>
                        <div class="col-desc text-center">
                            <h4>Learning Lorem</h4>
                            <p>Responsive interface Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            <a href="">Learn More</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 p-0">
                    <div class="col-content">
                        <div class="feature-icon">
                            <div class="feat-img transform-50 afterclear text-center">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-f2.png" alt="">
                            </div>
                        </div>
                        <div class="col-desc text-center">
                            <h4>Learning Lorem</h4>
                            <p>Responsive interface Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            <a href="">Learn More</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 p-0">
                    <div class="col-content">
                        <div class="feature-icon">
                            <div class="feat-img transform-50 afterclear text-center">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-f3.png" alt="">
                            </div>
                        </div>
                        <div class="col-desc text-center">
                            <h4>Learning Lorem</h4>
                            <p>Responsive interface Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            <a href="">Learn More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-board section-advantage bg-gray animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-title text-center">
                <h3 class="animated fadeInUpShort">Advantages Lorem</h3>
                <p class="animated fadeInUpShort delay-250">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt </p>
            </div>
            <div class="section-top afterclear">
                <div class="col-md-8">
                    <div class="board-img animated fadeInDownShort">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/test-dashboard.png" alt="">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="section-title afterclear">
                        <div class="board-content transform-y">
                            <h4><?php the_field('workforce_mobile_title'); ?></h4>
                            <?php the_field('workforce_list'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-gap">
        <div class="gap-100"></div>
    </div>
    <div class="section-content animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-request bg-inline bg-request animated fadeInUp">
                <div class="section-title text-center text-white">
                    <h3><?php the_field('request_title'); ?></h3>
                    <p><?php the_field('request_content'); ?></p>
                    <div class="gap-30"></div>
                    <a href="#request" data-toggle="modal" class="btn-common"><?php the_field('request_button_text'); ?></a>
                </div>
            </div>
        </div>
    </div>
<?php
    endwhile; else :
endif;
get_footer();