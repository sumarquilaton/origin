<?php
/**
 * Template Name: Benefits
 *
 *
 */

get_header();

global $post;
$post_slug=$post->post_name;

$bg_img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );

if ( have_posts() ) : while ( have_posts() ) : the_post();
?>
    <div class="section-banner animatedParent animateOnce">
        <div class="banner-home bg-inline" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/banner-benefits.png');">
            <div class="benefits-banner banner-desktop">
                <div class="benefits-img animated fadeInDownShort go">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/benefits-img.png" alt="">
                </div>
            </div>
            <div class="banner-mobile">
               <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/benefits-mobile.png" alt="">
            </div>
            <div class="container-fluid">
                <div class="banner-content transform-y">
                    <h2 class="animated fadeInUpShort go"><?php the_field('heading'); ?></h2>
                    <?php the_field('content'); ?>
                    <div class="gap-30"></div>
                    <div class="animated fadeInUpShort delay-250 go">
                        <a href="#lightbox" data-toggle="modal" class="btn-common"><?php the_field('button_text'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-sub-banner bg-white animatedParent animateOnce">
        <div class="container-fluid">
            <div class="col-md-7">
                <div class="benefits-grid afterclear">
                    <?php while (have_rows('benefits_list')): the_row(); ?>
                        <div class="col-md-6 col-sm-6">
                            <div class="benefits-col animated fadeInDownShort">
                                <div class="benefits-icon">
                                    <img src="<?php the_sub_field('benefits_icon'); ?>" alt="">
                                </div>
                                <h4><?php the_sub_field('b_title'); ?></h4>
                                <p><?php the_sub_field('b_content'); ?></p>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
            <div class="col-md-5">
                <div class="section-title transform-y">
                    <h3 class="animated fadeInUpShort"><?php the_field('benefits_title'); ?></h3>
                    <div class="animated fadeInUpShort delay-250">
                        <?php the_field('benefits_content'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content bg-gray animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-title text-center">
                <h3 class="animated fadeInUpShort"><?php the_field('management_title'); ?></h3>
                <div class="animated fadeInUpShort delay-250">
                    <?php the_field('management_content'); ?>
                </div>
            </div>
            <div class="circle-grid section-top afterclear animated">
                <?php while (have_rows('management_list')): the_row(); ?>
                    <div class="col-md-3 col-sm-6">
                        <div class="circle-list text-center">
                            <div class="circle-icon">
                                <div class="circle-img transform-50">
                                    <img src="<?php the_sub_field('management_icon'); ?>" alt="">
                                </div>
                            </div>
                            <h4><?php the_sub_field('m_title'); ?></h4>
                            <p><?php the_sub_field('m_content'); ?></p>
                        </div>
                    </div>
                <?php endwhile; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-sub-banner bg-white animatedParent animateOnce">
        <div class="container-fluid">
            <div class="col-md-7">
                <div class="employee-grid">
                    <div class="employee-img employee-1 animated fadeInDownShort">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/employ-1.png" alt="">
                    </div>
                    <div class="employee-img employee-2 animated fadeInDownShort delay-250">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/employ-2.png" alt="">
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="section-title transform-y">
                    <h3 class="animated fadeInUpShort"><?php the_field('employee_title'); ?></h3>
                    <div class="animated fadeInUpShort delay-250">
                        <?php the_field('employee_content'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-gap pt-0">
        <div class="gap-100"></div>
    </div>
    <div class="section-content animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-request bg-inline bg-request animated fadeInUp">
                <div class="section-title text-center text-white">
                    <h3><?php the_field('request_title'); ?></h3>
                    <p><?php the_field('request_content'); ?></p>
                    <div class="gap-30"></div>
                    <a href="#request" data-toggle="modal" class="btn-common"><?php the_field('request_button_text'); ?></a>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-gallery modal fade" tabindex="-1" id="lightbox" role="dialog">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <div class="modal-dialog">
        <div class="modal-content animated fadeInUpShort go">
            <div class="modal-body">
                <iframe width="50%" height="450" src="<?php the_field('video_link',7); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php
    endwhile; else :
endif;
get_footer(); ?>