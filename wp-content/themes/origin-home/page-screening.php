<?php
/**
 * Template Name: Background Screening
 *
 *
 */

get_header();

global $post;
$post_slug=$post->post_name;

$bg_img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );

if ( have_posts() ) : while ( have_posts() ) : the_post();
?>
    <div class="section-content section-sub-banner animatedParent animateOnce pt-4">
        <div class="container-fluid">
            <div class="col-md-6 pull-right">
                <div class="screening-img banner-desktop animated fadeInDownShort go">
                    <?php include_once "inc/vector.php"?>
                </div>
                <div class="banner-mobile">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/screening-mobile.png" alt="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="section-title transform-y">
                    <h3 class="animated fadeInUpShort go"><?php the_field('heading'); ?></h3>
                    <?php the_field('content'); ?>
                    <div class="gap-30"></div>
                    <div class="animated fadeInUpShort delay-250 go">
                        <a href="#hrmanage" data-toggle="modal" class="btn-common">Watch Video</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-sub-banner animatedParent animateOnce">
        <div class="container-fluid">
            <div class="col-md-6">
                <div class="screening-img banner-desktop">
                    <?php include_once "inc/why.php"?>
                </div>
                <div class="banner-mobile">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/why-mobile.png" alt="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="section-title transform-y">
                    <h3 class="animated fadeInUpShort"><?php the_field('why_title'); ?></h3>
                    <p class="animated fadeInUpShort delay-250">
                        <?php the_field('why_content'); ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-sub-banner animatedParent animateOnce">
        <div class="container-fluid">
            <div class="col-md-6 pull-right">
                <div class="screening-img animated fadeInDownShort">
                    <img src="<?php the_field('graded_image'); ?>" alt="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="section-title transform-y">
                    <h3 class="animated fadeInUpShort"><?php the_field('graded_title'); ?></h3>
                    <p class="animated fadeInUpShort delay-250">
                        <?php the_field('graded_content'); ?>
                    </p>
                    <div class="gap-20"></div>
                    <div class="animated fadeInUpShort delay-250">
                        <a href="<?php the_field('button_link'); ?>" class="btn-common"><?php the_field('button_text'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content pt-0" id="zservices">
        <div class="container-fluid animatedParent animateOnce">
            <div class="section-title text-center">
                <h3 class="animated fadeInUpShort"><?php the_field('services_title'); ?></h3>
                <p class="animated fadeInUpShort delay-250"><?php the_field('services_content'); ?></p>
            </div>
            <div class="section-services section-top just-center afterclear animated fadeInUp">
            <?php
            $posts = query_posts(array(
                    'post_type' => 'services',
                    'order'		=> 'ASC',
                    'posts_per_page'      => -1,
            ));

            if ( have_posts() ) : ?>
                <?php $i=0; while(have_posts()) : the_post(); $i++; ?>
                <div class="col-md-3 col-sm-4">
                    <a href="<?php the_permalink(); ?>">
                        <div class="services-holder">
                            <div class="services-icon">
                                <img src="<?php the_field('services_icon'); ?>" alt="">
                            </div>
                            <div class="services-title">
                                <p><?php the_title(); ?></p>
                            </div>
                        </div>
                    </a>
                </div>
                <?php endwhile; wp_reset_query(); ?>
            <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="section-content section-sub-banner section-orig-adv animatedParent animateOnce">
        <div class="container-fluid">
            <div class="col-md-6">
                <div class="screening-img animated fadeInDownShort">
                    <img src="<?php the_field('advantage_image'); ?>" alt="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="section-title transform-y">
                    <h3 class="animated fadeInUpShort"><?php the_field('advantage_title'); ?></h3>
                    <p class="animated fadeInUpShort delay-250">
                        <?php the_field('advantage_content'); ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-sub-banner section-orig-adv section-workforce animatedParent animateOnce">
        <div class="container-fluid">
            <div class="col-md-6 pull-right">
                <div class="screening-img animated fadeInDownShort">
                    <video width="100%" height="335" controls poster="<?php echo get_template_directory_uri(); ?>/images/poster-1.jpg">
                        <source src="<?php the_field('expertise_video_link'); ?>" type="video/mp4">
                    </video>
                </div>
            </div>
            <div class="col-md-6">
                <div class="section-title transform-y">
                    <h3 class="animated fadeInUpShort"><?php the_field('expertise_title'); ?></h3>
                    <?php the_field('expertise_content'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-gap pt-0">
        <div class="gap-100"></div>
    </div>
    <div class="section-content animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-request bg-inline bg-request animated fadeInUp">
                <div class="section-title text-center text-white">
                    <h3><?php the_field('request_title'); ?></h3>
                    <p><?php the_field('request_content'); ?></p>
                    <div class="gap-30"></div>
                    <a href="#request" data-toggle="modal" class="btn-common"><?php the_field('request_button_text'); ?></a>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-gallery modal fade" id="hrmanage">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body animated fadeInUpShort go">
                    <video width="50%" height="450" controls poster="<?php echo get_template_directory_uri(); ?>/images/poster-1.jpg">
                        <source src="<?php the_field('expertise_video_link'); ?>" type="video/mp4">
                    </video>
                </div><!-- /.modal-body -->
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php
    endwhile; else :
endif;
get_footer();
