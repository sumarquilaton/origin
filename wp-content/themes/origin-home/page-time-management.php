<?php
/**
 * Template Name: Time Management
 *
 *
 */

get_header();

global $post;
$post_slug=$post->post_name;

$bg_img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );

if ( have_posts() ) : while ( have_posts() ) : the_post();
?>
    <div class="section-banner animatedParent animateOnce">
        <div class="banner-home bg-inline" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/time-banner-2.png');">
            <div class="time-banner banner-desktop">
                <div class="time-img animated fadeInDownShort go">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/time-image.png" alt="">
                </div>
            </div>
            <div class="banner-mobile">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/time-image.png" alt="">
            </div>
            <div class="container-fluid">
                <div class="banner-content transform-y">
                    <h2 class="animated fadeInUpShort go"><?php the_field('heading'); ?></h2>
                    <?php the_field('content'); ?>
                    <div class="gap-30"></div>
                    <div class="animated fadeInUpShort delay-250 go">
                        <a href="#lightbox" data-toggle="modal" class="btn-common"><?php the_field('button_text'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content pt-0 animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-laptop animated fadeInDownShort">
                <img src="<?php the_field('time_management_image'); ?>" alt="">
            </div>
            <div class="laptop-content-list afterclear animated fadeInUp">
                <?php while (have_rows('time_management_list')): the_row(); ?>
                <div class="col-md-3">
                    <div class="icon-list">
                        <div class="icon-img">
                            <img src="<?php the_sub_field('time_image'); ?>" alt="">
                        </div>
                        <div class="icon-title">
                            <h4><?php the_sub_field('time_title'); ?></h4>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
    <div class="section-content section-sub-banner section-timeoff pt-0 animatedParent animateOnce">
        <div class="container-fluid">
            <div class="col-md-6">
                <div class="screening-img animated fadeInDownShort">
                    <img src="<?php the_field('time-off_image'); ?>" alt="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="section-title transform-y">
                    <h3 class="animated fadeInUpShort"><?php the_field('time-off'); ?></h3>
                    <div class="animated fadeInUpShort delay-250 go">
                        <?php the_field('time-off_content'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-sub-banner section-time-off section-parent-origin animatedParent animateOnce">
        <div class="container-fluid">
            <div class="col-md-6 pull-right">
                <div class="screening-img animated fadeInDownShort">
                    <img src="<?php the_field('time_and_attendance_image'); ?>" alt="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="section-title transform-y">
                    <h3 class="animated fadeInUpShort"><?php the_field('time_and_attendance_title'); ?></h3>
                    <div class="animated fadeInUpShort delay-250 go">
                        <?php the_field('time_and_attendance_content'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-request bg-inline bg-request animated fadeInUp">
                <div class="section-title text-center text-white">
                    <h3><?php the_field('request_title'); ?></h3>
                    <p><?php the_field('request_content'); ?></p>
                    <div class="gap-30"></div>
                    <a href="#request" data-toggle="modal" class="btn-common"><?php the_field('request_button_text'); ?></a>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-gallery modal fade" tabindex="-1" id="lightbox" role="dialog">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-dialog">
            <div class="modal-content animated fadeInUpShort go">
                <div class="modal-body">
                    <iframe width="50%" height="450" src="<?php the_field('video_link',7); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div><!-- /.modal-body -->
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php
    endwhile; else :
endif;
get_footer();