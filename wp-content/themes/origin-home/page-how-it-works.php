<?php
/**
 * Template Name: How it Works
 *
 *
 */

get_header();

global $post;
$post_slug=$post->post_name;

$bg_img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );

if ( have_posts() ) : while ( have_posts() ) : the_post();
?>

    <div class="section-content pb-12 bg-inline animatedParent animateOnce" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/banner-how-it-works.png')">
        <div class="container-fluid">
            <div class="section-title section-mw500 text-white text-center">
                <h2 class="animated fadeInUpShort go"><?php the_field('heading'); ?></h2>
                <?php the_field('content'); ?>
                <div class="gap-20"></div>
                <div class="animated fadeInUpShort delay-250 go">
                    <a href="#lightbox" data-toggle="modal" class="btn-common"><?php the_field('button_text'); ?></a>
                </div>
            </div>
        </div>
    </div>
    <div class="container-relative mn-8 animatedParent animateOnce">
        <div class="container-fluid">
            <div class="process-column animated fadeInUp go">
                <div class="process-list afterclear">
                    <?php while (have_rows('process_list')): the_row(); ?>
                        <div class="col-md-4 col-sm-4 p-0">
                            <div class="process-col">
                                <div class="process-number fl-left">
                                    <h4><?php the_sub_field('process_number'); ?></h4>
                                </div>
                                <div class="process-desc">
                                    <h5><?php the_sub_field('process_title'); ?></h5>
                                    <p><?php the_sub_field('process_description'); ?></p>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-sub-banner bg-white animatedParent animateOnce">
        <div class="container-fluid">
            <div class="col-md-6">
                <div class="screening-img animated fadeInDownShort">
                    <img src="<?php the_field('process_image'); ?>" alt="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="section-title pl-2 transform-y">
                    <div class="title-row afterclear animated fadeInUpShort">
                        <div class="title-number transform-y">
                            <h4>1</h4>
                        </div>
                        <h3><?php the_field('proc_title'); ?></h3>
                    </div>
                    <div class="animated fadeInUpShort delay-250">
                        <?php the_field('proc_content'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-sub-banner animatedParent animateOnce">
        <div class="container-fluid">
            <div class="col-md-6 pull-right">
                <div class="screening-img animated fadeInDownShort">
                    <img src="<?php the_field('on_board_image'); ?>" alt="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="section-title transform-y">
                    <div class="title-row afterclear animated fadeInUpShort">
                        <div class="title-number transform-y">
                            <h4>2</h4>
                        </div>
                        <h3><?php the_field('on_board_title'); ?></h3>
                    </div>
                    <div class="animated fadeInUpShort delay-250">
                        <?php the_field('on_board_content'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-sub-banner animatedParent animateOnce">
        <div class="container-fluid">
            <div class="col-md-6">
                <div class="screening-img animated fadeInDownShort">
                    <img src="<?php the_field('engagement_image'); ?>" alt="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="section-title pl-2 transform-y">
                    <div class="title-row afterclear animated fadeInUpShort">
                        <div class="title-number transform-y">
                            <h4>3</h4>
                        </div>
                        <h3><?php the_field('engagement_title'); ?></h3>
                    </div>
                    <div class="animated fadeInUpShort delay-250">
                        <?php the_field('engagement_content'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content bg-gray section-parent-origin animatedParent animateOnce">
        <div class="container-fluid">
            <div class="circle-grid circle-how afterclear animated fadeInUpShort">
                <?php while (have_rows('engagement_process')): the_row(); ?>
                    <div class="col-md-3">
                        <div class="circle-list text-center">
                            <div class="circle-icon">
                                <div class="circle-img transform-50">
                                    <img src="<?php the_sub_field('engagement_icon'); ?>" alt="">
                                </div>
                            </div>
                            <h4><?php the_sub_field('engage_title'); ?></h4>
                            <?php if( get_field('engage_content') ): ?>
                                <p><?php the_sub_field('engage_content'); ?></p>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
    <div class="section-content animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-request bg-inline bg-request animated fadeInUp">
                <div class="section-title text-center text-white">
                    <h3><?php the_field('request_title'); ?></h3>
                    <p><?php the_field('request_content'); ?></p>
                    <div class="gap-30"></div>
                    <a href="#request" data-toggle="modal" class="btn-common"><?php the_field('request_button_text'); ?></a>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-gallery modal fade" tabindex="-1" id="lightbox" role="dialog">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-dialog">
            <div class="modal-content animated fadeInUpShort go">
                <div class="modal-body">
                    <iframe width="50%" height="450" src="<?php the_field('video_link',7); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div><!-- /.modal-body -->
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php
    endwhile; else :
endif;
get_footer();