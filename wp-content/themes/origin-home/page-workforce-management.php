<?php
/**
 * Template Name: Workforce Management
 *
 *
 */

get_header();

global $post;
$post_slug=$post->post_name;

$bg_img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );

if ( have_posts() ) : while ( have_posts() ) : the_post();
?>
    <div class="section-content section-sub-banner pt-4 animatedParent animateOnce">
        <div class="container-fluid">
            <div class="col-md-6 pull-right">
                <div class="screening-img animated fadeInDownShort go">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/workforce-2.png" alt="">
                     <?php //include_once "inc/workforce.php"?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="section-title transform-y">
                    <h3 class="animated fadeInUpShort go"><?php the_field('heading'); ?></h3>
                    <?php the_field('content'); ?>
                    <div class="gap-30"></div>
                    <div class="animated fadeInUpShort delay-250 go">
                        <a href="#hrmanage" data-toggle="modal" class="btn-common">Watch Video</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content pt-0 animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-title mw-540 text-center">
                <h3 class="animated fadeInUpShort"><?php the_field('experience_title'); ?></h3>
                <div class="animated fadeInUpShort delay-250">
                    <?php the_field('experience_content'); ?>
                    <?php get_template_part('origin-experience-svg')?>
                </div>
            </div>
        </div>
     </div>
    <div class="section-content bg-gray animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-title text-center">
                <h3 class="animated fadeInUpShort"><?php the_field('solutions_title'); ?></h3>
                <div class="animated fadeInUpShort delay-250">
                    <?php the_field('solutions_content'); ?>
                </div>
            </div>
            <div class="solutions-grid section-top afterclear animated fadeInUp">
                <?php while (have_rows('solutions_list')): the_row(); ?>
                    <div class="col-md-6 p-0">
                        <div class="sol-column afterclear">
                            <div class="sol-icon fl-left">
                                <div class="transform-50">
                                    <img src="<?php the_sub_field('solutions_icon'); ?>" alt="">
                                </div>
                            </div>
                            <div class="sol-desc">
                                <h5><?php the_sub_field('s_title'); ?></h5>
                                <p><?php the_sub_field('s_content'); ?></p>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
    <div class="section-content section-sub-banner section-workforce section-orig-adv animatedParent animateOnce">
        <div class="container-fluid">
            <div class="col-md-7 pull-right">
                <div class="screening-img animated fadeInDownShort">
                    <video width="100%" height="400px" controls poster="<?php echo get_template_directory_uri(); ?>/images/poster-2.jpg">
                        <source src="<?php the_field('video_file'); ?>" type="video/mp4">
                    </video>
                </div>
            </div>
            <div class="col-md-5 animated fadeInUpShort">
                <?php while (have_rows('workforce_list')): the_row(); ?>
                <div class="col-md-6 col-sm-6">
                    <div class="work-list">
                        <div class="work-icon">
                            <img src="<?php the_sub_field('workforce_icon'); ?>" alt="">
                        </div>
                        <div class="work-desc">
                            <h5><?php the_sub_field('w_title'); ?></h5>
                            <p><?php the_sub_field('w_content'); ?></p>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
    <div class="section-content section-gap pt-0">
        <div class="gap-100"></div>
    </div>
    <div class="section-content animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-request bg-inline bg-request animated fadeInUp">
                <div class="section-title text-center text-white">
                    <h3><?php the_field('request_title'); ?></h3>
                    <p><?php the_field('request_content'); ?></p>
                    <div class="gap-30"></div>
                    <a href="#request" data-toggle="modal" class="btn-common"><?php the_field('request_button_text'); ?></a>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-gallery modal fade" id="hrmanage">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body animated fadeInUpShort go">
                    <video width="50%" height="450" controls poster="<?php echo get_template_directory_uri(); ?>/images/poster-2.jpg">
                        <source src="<?php the_field('video_file'); ?>" type="video/mp4">
                    </video>
                </div><!-- /.modal-body -->
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php
    endwhile; else :
endif;
get_footer();