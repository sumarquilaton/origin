<div class="section-content section-footer pt-0 animatedParent animateOnce">
    <div class="container-fluid animated fadeInUp delay-250">
        <div class="col-md-3 col-sm-3 p-0">
            <div class="footer-logo">
                <a href="<?php echo site_url(); ?>">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" alt="">
                </a>
            </div>
            <div class="gap-20"></div>
            <div class="footer-copyright">
                <p>© Origin  2018</p>
            </div>
        </div>
        <div class="col-md-7 col-sm-7 p-0">
            <div class="col-md-3 col-sm-3 p-0">
                <div class="footer-list">
                    <h4>Company</h4>
                    <?php
                    wp_nav_menu( array(
                        'menu'              => 'Company',
                        'theme_location'    => 'company',
                        'container'         => false,
                        'menu_class'        => 'footer-menu')
                    );
                    ?>
                </div>
            </div>
            <div class="col-md-9 col-sm-9 p-0">
                <div class="footer-list">
                    <h4>Our Platform</h4>
                    <?php
                    wp_nav_menu( array(
                        'menu'              => 'Our Platform',
                        'theme_location'    => 'our-platform',
                        'container'         => false,
                        'menu_class'        => 'footer-menu')
                    );
                    ?>
                </div>
            </div>
        </div>
        <div class="col-md-2 col-sm-2 p-0">
            <div class="footer-list">
                <h4>Contact Us</h4>
                <ul>
                    <li><a href="tel:<?php the_field('telephone_number', 'option'); ?>"><?php the_field('telephone_number', 'option'); ?></a></li>
                    <li><a href="https://<?php the_field('website_link', 'option'); ?>" target="_blank"><?php the_field('website_link', 'option'); ?></a></li>
                </ul>
                <div class="social-icons">
                    <ul>
                        <li>
                            <a href="<?php the_field('twitter_link', 'option'); ?>" target="_blank"><i class="transform-50 fa fa-twitter" aria-hidden="true"></i></a>
                        </li>
                        <li>
                            <a href="<?php the_field('google_plus_link', 'option'); ?>" target="_blank"><i class="transform-50 fa fa-google-plus" aria-hidden="true"></i></a>
                        </li>
                        <li>
                            <a href="<?php the_field('linkedin_link', 'option'); ?>" target="_blank"><i class="transform-50 fa fa-linkedin" aria-hidden="true"></i></a>
                        </li>
                        <li>
                            <a href="<?php the_field('facebook_link', 'option'); ?>" target="_blank"><i class="transform-50 fa fa-facebook" aria-hidden="true"></i></a>
                        </li>
                        <li>
                            <a href="<?php the_field('youtube_link', 'option'); ?>" target="_blank"><i class="transform-50 fa fa-youtube" aria-hidden="true"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-gallery modal fade" id="request">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="request-form afterclear animated fadeInUpShort go">
                        <?php echo do_shortcode('[contact-form-7 id="16576" title="Demo"]'); ?>
                    </div>
                </div>
            </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php wp_footer(); ?>

</body>
</html>
