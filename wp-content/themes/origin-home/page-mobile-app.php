<?php
/**
 * Template Name: Mobile App
 *
 *
 */

get_header();

global $post;
$post_slug=$post->post_name;

$bg_img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );

if ( have_posts() ) : while ( have_posts() ) : the_post();
?>
    <div class="section-banner animatedParent animateOnce">
        <div class="banner-home bg-inline" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/banner-mobile-2.png');">
            <div class="mobile-banner banner-desktop">
                <div class="banner-mob banner-mob-1 animated fadeInDownShort go">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mobile-ban-1.png" alt="">
                </div>
                <div class="banner-mob banner-mob-2 animated fadeInDownShort delay-250 go">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mobile-ban-2.png" alt="">
                </div>
            </div>
            <div class="banner-mobile">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/app-mobile.png" alt="">
            </div>
            <div class="container-fluid">
                <div class="banner-content transform-y">
                    <h2 class="animated fadeInUpShort go"><?php the_field('heading'); ?></h2>
                    <?php the_field('content'); ?>
                    <div class="gap-30"></div>
                    <div class="animated fadeInUpShort delay-250 go">
                        <a href="#lightbox" data-toggle="modal" class="btn-common"><?php the_field('button_text'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-sub-banner bg-white animatedParent animateOnce">
        <div class="container-fluid">
            <div class="col-md-7">
                <div class="screening-img animated">
                    <div class="spiral-img banner-desktop">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/spiral.png" alt="">
                        
                        <div class="mobile-holder">
                            <div class="mobile mobile-1 animated fadeInDownShort">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mobile-1.png" alt="">
                            </div>
                            <div class="mobile mobile-2 animated fadeInUpShort delay-250">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mobile-2.png" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="banner-mobile">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mobile-request.png" alt="">
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="section-title transform-y">
                    <h3 class="animated fadeInUpShort"><?php the_field('hr_wherever_title'); ?></h3>
                    <div class="animated fadeInUpShort delay-250">
                        <?php the_field('hr_wherever_content'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content bg-gray animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-title text-center">
                <h3 class="animated fadeInUpShort"><?php the_field('feature_title'); ?></h3>
                <div class="animated fadeInUpShort delay-250">
                    <?php the_field('feature_content'); ?>
                </div>
            </div>
            <div class="section-cols four-cols section-top features-container just-center afterclear animated fadeInUp">
                <?php while (have_rows('feature_list')): the_row(); ?>
                    <div class="col-md-4">
                        <div class="col-content">
                            <div class="col-img text-center">
                                <img src="<?php the_sub_field('feature_icon'); ?>" alt="">
                            </div>
                            <div class="col-desc text-center">
                                <h4><?php the_sub_field('f_title'); ?></h4>
                                <p><?php the_sub_field('f_content'); ?></p>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
    <div class="section-content section-sub-banner section-started bg-white animatedParent animateOnce">
        <div class="container-fluid">
            <div class="col-md-6">
                <div class="screening-img animated">
                    <div class="mobile-phone">
                        <div class="mobile mobile-3 animated fadeInDownShort">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mobile-3.png" alt="">
                        </div>
                        <div class="mobile mobile-4 animated fadeInUpShort">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mobile-4.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="section-title transform-y">
                    <h3 class="animated fadeInUpShort"><?php the_field('started_title'); ?></h3>
                    <div class="animated fadeInUpShort delay-250">
                        <?php the_field('started_content'); ?>
                    </div>
                    <div class="store-links animated fadeInUp">
                        <?php while (have_rows('store_links')): the_row(); ?>
                            <div class="store">
                                <a href="<?php the_sub_field('store_link'); ?>">
                                    <img src="<?php the_sub_field('store_image'); ?>" alt="">
                                </a>
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-mobile-gap pt-0"></div>
    <div class="section-content section-gap pt-0">
        <div class="gap-100"></div>
    </div>
    <div class="section-content animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-request bg-inline bg-request animated fadeInUp">
                <div class="section-title text-center text-white">
                    <h3><?php the_field('request_title'); ?></h3>
                    <p><?php the_field('request_content'); ?></p>
                    <div class="gap-30"></div>
                    <a href="#request" data-toggle="modal" class="btn-common"><?php the_field('request_button_text'); ?></a>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-gallery modal fade" tabindex="-1" id="lightbox" role="dialog">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-dialog">
            <div class="modal-content animated fadeInUpShort go">
                <div class="modal-body">
                    <iframe width="50%" height="450" src="<?php the_field('video_link',7); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div><!-- /.modal-body -->
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php
    endwhile; else :
endif;
get_footer();