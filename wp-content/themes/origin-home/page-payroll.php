<?php
/**
 * Template Name: Payroll
 *
 *
 */

get_header();

global $post;
$post_slug=$post->post_name;

$bg_img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );

if ( have_posts() ) : while ( have_posts() ) : the_post();
?>

    <div class="section-content section-bg-payroll bg-inline animatedParent animateOnce" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/banner-payroll.png')">
        <div class="container-fluid">
            <div class="section-title section-mw500 text-white text-center">
                <h2 class="animated fadeInUpShort go"><?php the_field('heading'); ?></h2>
                <?php the_field('content'); ?>
            </div>
        </div>
    </div>
    <div class="container-relative mn-23 animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-payroll text-center">
                <div class="payroll payroll-1 animated fadeInDownShort go">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/payroll-1.png" alt="">
                </div>
                <div class="payroll payroll-2 animated fadeInDownShort delay-250 go">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/payroll-2.png" alt="">
                </div>
            </div>
        </div>
    </div>
    <div class="section-content animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-title text-center">
                <h3 class="animated fadeInUpShort"><?php the_field('feat_title'); ?></h3>
                <div class="animated fadeInUpShort delay-250">
                    <?php the_field('feat_content'); ?>
                </div>
            </div>
            <div class="section-top afterclear">
                <div class="col-md-6">
                    <div class="feature-img animated fadeInDownShort">
                        <img src="<?php the_field('feat_image'); ?>" alt="">
                    </div>
                </div>
                <div class="col-md-6 animated fadeInUpShort">
                    <div class="feature-section">
                        <?php while (have_rows('feat_list')): the_row(); ?>
                            <div class="feature-list afterclear">
                                <div class="feat-icon fl-left">
                                    <img src="<?php the_sub_field('feat_icon'); ?>" alt="">
                                </div>
                                <div class="feature-desc">
                                    <p><?php the_sub_field('feat_title'); ?></p>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content bg-gray animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-title text-center">
                <h3 class="animated fadeInUpShort"><?php the_field('solutions_title'); ?></h3>
                <div class="animated fadeInUpShort delay-250">
                    <?php the_field('solutions_content'); ?>
                </div>
            </div>
            <div class="payroll-solutions section-top afterclear animated fadeInUp">
                <?php while (have_rows('solutions_list')): the_row(); ?>
                    <div class="col-md-4">
                        <div class="ps-list text-center">
                            <div class="ps-icon">
                                <div class="ps-img transform-50">
                                    <img src="<?php the_sub_field('solutions_icon'); ?>" alt="">
                                </div>
                            </div>
                            <div class="ps-details">
                                <h4><?php the_sub_field('solutions_title'); ?></h4>
                                <p><?php the_sub_field('solutions_content'); ?></p>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
    <div class="section-content section-sub-banner bg-white pt-16 animatedParent animateOnce">
        <div class="container-fluid">
            <div class="col-md-4 pull-right">
                <div class="section-title transform-y">
                    <h3 class="animated fadeInUpShort"><?php the_field('benefits_title'); ?></h3>
                    <div class="animated fadeInUpShort delay-250">
                        <?php the_field('benefits_content'); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-8 p-0">
                <div class="benefits-payroll bg-inline afterclear animated fadeInUp" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/bg-benefits.png'); ">
                    <?php while (have_rows('benefits_grid')): the_row(); ?>
                        <div class="col-md-4 col-sm-6">
                            <?php if( get_sub_field('benefits_grid_icon') ): ?>
                            <div class="bene-list text-center text-white">
                                <div class="bene-icon">
                                    <img src="<?php the_sub_field('benefits_grid_icon'); ?>" alt="">
                                </div>
                                <div class="bene-desc">
                                    <h4><?php the_sub_field('benefits_grid_title'); ?></h4>
                                    <p><?php the_sub_field('benefits_grid_content'); ?></p>
                                </div>
                            </div>
                            <?php endif; ?>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-gap pt-0">
        <div class="gap-100"></div>
    </div>
    <div class="section-content animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-request bg-inline bg-request animated fadeInUp">
                <div class="section-title text-center text-white">
                    <h3><?php the_field('request_title'); ?></h3>
                    <p><?php the_field('request_content'); ?></p>
                    <div class="gap-30"></div>
                    <a href="#request" data-toggle="modal" class="btn-common"><?php the_field('request_button_text'); ?></a>
                </div>
            </div>
        </div>
    </div>
<?php
    endwhile; else :
endif;
get_footer();