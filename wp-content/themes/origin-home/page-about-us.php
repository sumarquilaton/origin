<?php
/**
 * Template Name: About Us
 *
 *
 */

get_header();

if ( have_posts() ) : while ( have_posts() ) : the_post();
    ?>
    <div class="section-banner animatedParent animateOnce">
        <div class="banner-home bg-inline" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/banner-about-us.png');">
            <div class="occupation-banner banner-desktop">
                <div class="about-img animated fadeInDownShort go">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/about-img.png" alt="">
                </div>
            </div>
            <div class="banner-mobile">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/about-img.png" alt="">
            </div>
            <div class="container-fluid">
                <div class="banner-content transform-y">
                    <h2 class="animated fadeInUpShort go"><?php the_field('heading'); ?></h2>
                    <?php the_field('content'); ?>
                    <div class="gap-30"></div>
                    <div class="animated fadeInUpShort delay-250 go">
                        <a href="#lightbox" data-toggle="modal" class="btn-common"><?php the_field('button_text'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-sub-banner section-overview-us bg-white pt-0 animatedParent animateOnce">
        <div class="container-fluid">
            <div class="col-md-6">
                <div class="screening-img animated fadeInDownShort">
                    <img src="<?php the_field('overview_image') ;?>" alt="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="section-title transform-y pl-3">
                    <h3 class="animated fadeInUpShort"><?php the_field('overview_text') ;?></h3>
                    <div class="animated fadeInUpShort delay-250">
                        <?php the_field('overview_content') ;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-sub-banner section-careers bg-white animatedParent animateOnce">
        <div class="container-fluid">
            <div class="col-md-6 pull-right">
                <div class="screening-img animated fadeInDownShort">
                    <img src="<?php the_field('careers_image') ;?>" alt="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="section-title transform-y pl-3">
                    <h3 class="animated fadeInUpShort"><?php the_field('careers_text') ;?></h3>
                    <div class="animated fadeInUpShort delay-250">
                        <?php the_field('careers_content') ;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-sub-banner section-overview-us bg-white animatedParent animateOnce">
        <div class="container-fluid">
            <div class="col-md-6">
                <div class="screening-img animated fadeInDownShort">
                    <img src="<?php the_field('client_image') ;?>" alt="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="section-title transform-y pl-3">
                    <h3 class="animated fadeInUpShort"><?php the_field('client_title') ;?></h3>
                    <div class="animated fadeInUpShort delay-250">
                        <?php the_field('client_content') ;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-title text-center">
                <h3 class="animated fadeInUpShort"><?php the_field('services_text') ;?></h3>
<!--                <div class="animated fadeInUpShort delay-250">-->
<!--                    --><?php //the_field('services_content') ;?>
<!--                </div>-->
            </div>
            <div class="section-cols four-cols section-top features-container afterclear animated fadeInUp">
                <?php while (have_rows('services_list')): the_row(); ?>
                    <div class="col-md-4 col-sm-6">
                        <a href="<?php echo site_url(); ?>/our-platform/hr-management/">
                            <div class="col-content">
                                <div class="col-img text-center">
                                    <img src="<?php the_sub_field('services_icon'); ?>" alt="">
                                </div>
                                <div class="col-desc text-center">
                                    <h4><?php the_sub_field('services_title'); ?></h4>
                                    <p><?php the_sub_field('services_description'); ?></p>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
    <div class="section-content bg-gray section-parent-origin animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-title text-center">
                <h3 class="animated fadeInUpShort"><?php the_field('news') ;?></h3>
                <div class="animated fadeInUpShort delay-250">
                    <?php the_field('news_content') ;?>
                </div>
                <div class="gap-20"></div>
                <a href="#subscribe" data-toggle="modal" class="btn-subscribe btn-common">Subscribe</a>
            </div>
            <div class="payroll-solutions news-content section-top just-center afterclear animated fadeInUp">

                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <?php
                        $posts = query_posts(array(
                                'post_type' => 'post',
                                'order'		=> 'ASC',
                                'posts_per_page'      => -1,
                        ));

                        if ( have_posts() ) : ?>
                            <?php $i=0; while(have_posts()) : the_post(); $i++; ?>
                                <?php if ($i % 3 == 1) : ?>
                                <div class="item <?php echo ($i == 1) ? 'active' : '' ?>">
                                <?php endif; ?>
                                    <div class="col-md-4 col-sm-6">
                                        <a href="<?php the_permalink(); ?>">
                                            <div class="ps-list scale-holder text-center">
                                                <div class="ps-icon">
                                                    <div class="ps-img bg-inline" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));?>'); ">
                                                    </div>
                                                </div>
                                                <div class="ps-details">
                                                    <p>
                                                        <?php
                                                        $content = get_the_content();
                                                        $content_filter = wp_filter_nohtml_kses( $content );
                                                        echo mb_strimwidth($content_filter, 0, 145, '...')
                                                        ?>
                                                    </p>
                                                </div>
                                                <div class="ps-sub-heading">
                                                    <?php global $post;
                                                    $terms = wp_get_post_terms( $post->ID, 'category');
                                                    ?>
                                                    <p><?php echo $terms[0]->name; ?></p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                <?php if ( $i % 3 == 0 || count($posts) == $i) : ?>
                                </div>
                                <?php endif; ?>
                            <?php endwhile; wp_reset_query(); ?>
                        <?php endif; ?>
                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <div class="text-center">
                    <div class="gap-30"></div>
                    <a href="<?php echo site_url(); ?>/blog/" class="btn-common">Learn More</a>
                </div>
            </div>
        </div>
        <div class="gap-50"></div>
    </div>
    <div class="section-content animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-request bg-inline bg-request animated fadeInUp">
                <div class="section-title text-center text-white">
                    <h3><?php the_field('request_title'); ?></h3>
                    <p><?php the_field('request_content'); ?></p>
                    <div class="gap-30"></div>
                    <a href="#request" data-toggle="modal" class="btn-common"><?php the_field('request_button_text'); ?></a>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-gallery modal fade" tabindex="-1" id="subscribe" role="dialog">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-dialog">
            <div class="modal-content animated fadeInUpShort go">
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="request-form afterclear animated fadeInUpShort go">
                            <?php echo do_shortcode('[contact-form-7 id="17620" title="Subscription"]');?>
                        </div>
                    </div>
                </div><!-- /.modal-body -->
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal-gallery modal fade" tabindex="-1" id="lightbox" role="dialog">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-dialog">
            <div class="modal-content animated fadeInUpShort go">
                <div class="modal-body">
                    <iframe width="50%" height="450" src="<?php the_field('video_link',7); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div><!-- /.modal-body -->
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <?php
endwhile; else :
endif;
get_footer();