<?php
/**
 * Template Name: Careers
 *
 *
 */

get_header();

global $post;
$post_slug=$post->post_name;

$bg_img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );

if ( have_posts() ) : while ( have_posts() ) : the_post();
?>
    <div class="section-content section-cb-careers bg-inline animatedParent animateOnce" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/careers-bg.png')">
        <div class="container-fluid">
            <div class="section-title section-mw500 text-center">
                <h2 class="animated fadeInUpShort go"><?php the_field('heading'); ?></h2>
                <?php the_field('content'); ?>
                <div class="gap-20"></div>
                <div class="animated fadeInUpShort delay-250 go">
                    <a href="#zcareers" class="btn-common"><?php the_field('button_text'); ?></a>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content bg-gray animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-title text-center">
                <h2 class="animated fadeInUpShort go"><?php the_field('exciting_title'); ?></h2>
                <?php the_field('exciting_content'); ?>
            </div>
        </div>
    </div>
    <div class="section-content animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-title section-mw500 text-center">
                <h2 class="animated fadeInUpShort go"><?php the_field('benefits_title'); ?></h2>
                <p><?php the_field('benefits_content'); ?>
                </p>
            </div>
            <div class="careers-benefits section-top afterclear">
                <?php while (have_rows('benefits_list')): the_row(); ?>
                    <div class="col-md-6">
                        <div class="cb-list">
                            <div class="cb-icon fl-left">
                                <img src="<?php the_sub_field('benefits_icon'); ?>" alt="">
                            </div>
                            <div class="cb-desc">
                                <p><?php the_sub_field('benefits_title'); ?></p>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
            <div class="gap-20"></div>
            <div class="animated fadeInUpShort delay-250 go text-center">
                <a href="#zcareers" class="btn-common">Apply Now</a>
            </div>
        </div>
    </div>
    <div class="section-content section-parent-origin pt-0 animatedParent animateOnce" id="zcareers">
        <div class="container-fluid">
            <div class="section-title text-center">
                <h2 class="animated fadeInUpShort go"><?php the_field('openings_title'); ?></h2>
            </div>
            <div class="section-top careers-openings">
                <iframe id="origin-careers" src="<?php the_field('iframe_link'); ?>" width="100%" height="1000" frameborder="0" scrolling="yes"></iframe>
            </div>
        </div>
    </div>
    <div class="section-content animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-request bg-inline bg-request animated fadeInUp">
                <div class="section-title text-center text-white">
                    <h3><?php the_field('request_title'); ?></h3>
                    <p><?php the_field('request_content'); ?></p>
                    <div class="gap-30"></div>
                    <a href="#request" data-toggle="modal" class="btn-common"><?php the_field('request_button_text'); ?></a>
                </div>
            </div>
        </div>
    </div>
<?php
    endwhile; else :
endif;
get_footer();