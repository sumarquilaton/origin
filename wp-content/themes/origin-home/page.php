<?php
/**
 * Default Template
 *
 *
 */

get_header();

global $post;
$post_slug=$post->post_name;

$bg_img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );

if ( have_posts() ) : while ( have_posts() ) : the_post();
?>
	<div class="section-content bg-inline animatedParent animateOnce" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/bg-request.png')">
		<div class="container-fluid">
			<div class="section-title section-mw500 text-white text-center">
				<h2 class="animated fadeInUpShort go"><?php the_field('heading'); ?></h2>
				<?php the_field('content'); ?>
			</div>
		</div>
	</div>
	<div class="section-content">
		<div class="container-fluid">

		</div>
	</div>
	<div class="section-content animatedParent animateOnce">
		<div class="container-fluid">
			<div class="section-request bg-inline bg-request animated fadeInUp">
				<div class="section-title text-center text-white">
					<h3><?php the_field('request_title'); ?></h3>
					<p><?php the_field('request_content'); ?></p>
					<div class="gap-30"></div>
					<a href="#request" data-toggle="modal" class="btn-common"><?php the_field('request_button_text',7); ?></a>
				</div>
			</div>
		</div>
	</div>

<?php
		endwhile; else :
	endif;
get_footer(); ?>