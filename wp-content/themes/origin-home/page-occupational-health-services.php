<?php
/**
 * Template Name: Occupational Health Services
 *
 *
 */

get_header();

global $post;
$post_slug=$post->post_name;

$bg_img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );

if ( have_posts() ) : while ( have_posts() ) : the_post();
?>
    <div class="section-banner animatedParent animateOnce">
        <div class="banner-home bg-inline" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/banner-occupational.png');">
            <div class="occupation-banner banner-desktop">
                <div class="occupation-img animated fadeInDownShort go">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/occupation.png" alt="">
                </div>
            </div>
            <div class="banner-mobile">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/occupation.png" alt="">
            </div>
            <div class="container-fluid">
                <div class="banner-content transform-y">
                    <h2 class="animated fadeInUpShort go"><?php the_field('heading'); ?></h2>
                    <?php the_field('content'); ?>
                    <div class="gap-30"></div>
                    <div class="animated fadeInUpShort delay-250 go">
                        <a href="#hrmanage" data-toggle="modal" class="btn-common"><?php the_field('button_text'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-sub-banner bg-white animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-title text-center">
                <h3 class="animated fadeInUpShort"><?php the_field('title_1'); ?></h3>
            </div>
            <div class="screening-img">
                <?php include_once "inc/occupational.php"?>
            </div>
       </div>
    </div>
    <div class="section-content section-sub-banner section-occupational bg-gray animatedParent animateOnce">
        <div class="container-fluid">
            <div class="col-md-6 pull-right">
                <div class="screening-img screen-video animated fadeInDownShort">
                    <video width="100%" height="335" controls poster="<?php echo get_template_directory_uri(); ?>/images/poster-13.jpg">
                        <source src="<?php the_field('video_file'); ?>" type="video/mp4">
                    </video>
                </div>
            </div>
            <div class="col-md-6">
                <div class="section-title transform-y">
                    <div class="testi-desc">
                        <p><?php the_field('origin_description'); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content bg-white animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-title text-center">
                <h3 class="animated fadeInUpShort"><?php the_field('section_title'); ?></h3>
                <p class="animated fadeInDownShort"><?php the_field('section_content'); ?></p>
            </div>
            <div class="section-safer section-top afterclear animated fadeInUp">
                <?php while (have_rows('program_list')): the_row(); ?>
                    <div class="col-md-6">
                        <div class="safer-list">
                            <div class="safer-icon">
                                <img src="<?php the_sub_field('program_image'); ?>" alt="">
                            </div>
                            <div class="transform-50">
                                <h4><?php the_sub_field('program_title'); ?></h4>
                                <?php the_sub_field('program_content'); ?>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
            <div class="animated fadeInUpShort delay-250 go text-center">
                <div class="gap-50"></div>
                <a href="<?php the_field('program_button_link'); ?>" class="btn-common"><?php the_field('program_button'); ?></a>
            </div>
        </div>
    </div>
    <div class="section-content section-sub-banner bg-white section-time-off section-testing animatedParent animateOnce">
        <div class="container-relative afterclear">
            <div class="col-md-7 p-0">
                <div class="screening-img animated fadeInUpShort">
                    <img src="<?php the_field('drug_testing_image'); ?>" alt="">
                </div>
            </div>
            <div class="col-md-5">
                <div class="section-title transform-y">
                    <h3 class="animated fadeInUpShort"><?php the_field('drug_testing_title'); ?></h3>
                    <?php the_field('drug_testing_content'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content pt-0 animatedParent animateOnce">
        <div class="container-fluid">
            <div class="urine-list drug-testing afterclear animated fadeInUp">
                <?php while (have_rows('drug_testing_list')): the_row(); ?>
                    <div class="col-md-3 p-0">
                        <p><?php the_sub_field('drug_testing_title'); ?></p>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
    <div class="section-content section-sub-banner bg-white pt-0 animatedParent animateOnce">
        <div class="container-fluid">
            <div class="col-md-6 pull-right">
                <div class="screening-img animated fadeInDownShort">
                    <img src="<?php the_field('physicals_image'); ?>" alt="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="section-title transform-y">
                    <h3 class="animated fadeInUpShort"><?php the_field('physicals_title'); ?></h3>
                    <?php the_field('physicals_content'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content pt-0 animatedParent animateOnce">
        <div class="container-fluid">
            <div class="urine-list afterclear animated fadeInUp">
                <?php while (have_rows('physicals_list')): the_row(); ?>
                    <div class="col-md-3 p-0">
                        <p><?php the_sub_field('physicals_title'); ?></p>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
    <div class="section-content bg-gray animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-title text-center">
                <h3 class="animated fadeInUpShort"><?php the_field('immunizations_title'); ?></h3>
                <p class="animated fadeInUpShort delay-250"><?php the_field('immunizations_content'); ?></p>
            </div>
            <div class="gap-30"></div>
            <div class="section-laptop animated fadeInUp">
                <img src="<?php the_field('immunizations_image'); ?>" alt="">
            </div>
            <div class="section-top">
                <div class="urine-list immunizations-list afterclear animated fadeInUp">
                    <?php while (have_rows('immunizations_list')): the_row(); ?>
                        <div class="col-md-3 p-0">
                            <p><?php the_sub_field('immunizations_title'); ?></p>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-sub-banner section-concentrate bg-white animatedParent animateOnce">
        <div class="container-fluid">
            <div class="col-md-6 pull-right">
                <div class="section-title transform-y">
                    <h3 class="animated fadeInUpShort mw-550"><?php the_field('concentrate_title'); ?></h3>
                    <p class="animated fadeInUpShort delay-250">
                        <?php the_field('concentrate_content'); ?>
                    </p>
                    <div class="animated fadeInUpShort delay-250 go">
                        <div class="gap-50"></div>
                        <a href="<?php the_field('concentrate_link'); ?>" class="btn-common"><?php the_field('concentrate_button'); ?></a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="concentrate-list afterclear">
                    <?php while (have_rows('concentrate_list')): the_row(); ?>
                        <div class="col-md-6 col-sm-6">
                            <div class="concentrate-content text-center">
                                <div class="concentrate-icon">
                                    <img src="<?php the_sub_field('concentrate_icon'); ?>" alt="">
                                </div>
                                <p><?php the_sub_field('concentrate_title'); ?></p>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
        <div class="section-top section-competencies afterclear">
            <div class="container-fluid">
                <div class="competencies-content afterclear">
                    <?php while (have_rows('competencies_list')): the_row(); ?>
                        <div class="col-md-6 col-sm-6">
                            <div class="competencies list">
                                <h4><?php the_sub_field('competencies_title'); ?></h4>
                                <p><?php the_sub_field('competencies_content'); ?></p>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-title text-center">
                <h3 class="animated fadeInUpShort"><?php the_field('samsha_certified_title'); ?></h3>
                <p class="animated fadeInUpShort delay-250"><?php the_field('samsha_certified_content'); ?></p>
            </div>
            <div class="section-top section-carousel animated fadeInUp">
                <div id="carousel-samsha" class="carousel slide" data-ride="carousel">

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner text-center" role="listbox">
                        <?php $i=0; while (have_rows('samsha_certified')): the_row(); $i++; ?>
                            <?php if ($i % 4 == 1) : ?>
                                <div class="item <?php echo ($i == 1) ? 'active' : '' ?>">
                            <?php endif; ?>
                            <div class="company-carousel">
                                <img src="<?php the_sub_field('samsha_certified_logo'); ?>" alt="">
                            </div>
                            <?php if ( $i % 4 == 0 ) : ?>
                                </div>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    </div>
                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-samsha" role="button" data-slide="prev">
                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-samsha" role="button" data-slide="next">
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-parent-origin pt-0 animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-title text-center">
                <h3 class="animated fadeInUpShort"><?php the_field('clinical_partners_title'); ?></h3>
                <p class="animated fadeInUpShort delay-250"><?php the_field('clinical_partners_content'); ?></p>
            </div>
            <div class="section-top section-carousel animated fadeInUp">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner text-center" role="listbox">
                        <?php $i=0; while (have_rows('clinical_partners')): the_row(); $i++; ?>
                            <?php if ($i % 4 == 1) : ?>
                                <div class="item <?php echo ($i == 1) ? 'active' : '' ?>">
                                <?php endif; ?>
                                    <div class="company-carousel">
                                        <img src="<?php the_sub_field('clinical_partners_logo'); ?>" alt="">
                                    </div>
                                    <?php if ( $i % 4 == 0 ) : ?>
                                </div>
                            <?php endif; ?>

                        <?php endwhile; ?>
                    </div>
                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-gap pt-0">
    </div>
    <div class="section-content animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-request bg-inline bg-request animated fadeInUp">
                <div class="section-title text-center text-white">
                    <h3><?php the_field('request_title'); ?></h3>
                    <p><?php the_field('request_content'); ?></p>
                    <div class="gap-30"></div>
                    <a href="#request" data-toggle="modal" class="btn-common"><?php the_field('request_button_text'); ?></a>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-gallery modal fade" id="hrmanage">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body animated fadeInUpShort go">
                    <video width="50%" height="450" controls poster="<?php echo get_template_directory_uri(); ?>/images/poster-13.jpg">
                        <source src="<?php the_field('video_file'); ?>" type="video/mp4">
                    </video>
                </div><!-- /.modal-body -->
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

<?php
    endwhile; else :
endif;
get_footer();