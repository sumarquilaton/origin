<?php
/**
 * Template Name: About Us
 *
 *
 */

get_header();

global $post;
$post_slug=$post->post_name;

$bg_img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );

if ( have_posts() ) : while ( have_posts() ) : the_post();
    ?>
    <div class="section-banner animatedParent animateOnce">
        <div class="banner-home bg-inline" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/banner-about-us.png');">
            <div class="occupation-banner">
                <div class="about-img animated fadeInDownShort go">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/about-img.png" alt="">
                </div>
            </div>
            <div class="container-fluid">
                <div class="banner-content transform-y">
                    <h2 class="animated fadeInUpShort go">About Us</h2>
                    <p class="animated fadeInUpShort delay-250 go">Lorem ipsum dolor sit amet, consectetur
                        <span>adipiscing elit. Aliquam </span></p>
                    <div class="gap-30"></div>
                    <div class="animated fadeInUpShort delay-250 go">
                        <a href="<?php the_field('button_link'); ?>" class="btn-common">Watch Video</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-sub-banner bg-white pt-0 animatedParent animateOnce">
        <div class="container-fluid">
            <div class="col-md-6">
                <div class="screening-img animated fadeInDownShort">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/overview.png" alt="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="section-title transform-y pl-3">
                    <h3 class="animated fadeInUpShort">Overview Lorem</h3>
                    <p class="animated fadeInUpShort delay-250">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu est eu ipsum faucibus dictum. Cras mattis ornare urna quis lacinia. Sed tristique eleifend maximus.
                    </p>
                    <p class="animated fadeInUpShort delay-250">
                        Maecenas ultrices justo in accumsan vestibulum. Duis quis justo arcu. Vivamus vel risus sit amet dolor aliquam luctus. Curabitur molestie blandit est a ultricies.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-overview-about animatedParent animateOnce">
        <div class="container-fluid">
            <div class="about-list afterclear animated fadeInUp go">
                <ul>
                    <?php while (have_rows('overview_list')): the_row(); ?>
                        <li>
                            <div class="icon-img">
                                <img src="<?php the_sub_field('list_image'); ?>" alt="">
                            </div>
                            <div class="icon-title text-center">
                                <h4><?php the_sub_field('list_title'); ?></h4>
                                <p><?php the_sub_field('list_content'); ?></p>
                            </div>
                        </li>
                    <?php endwhile; ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="section-content section-overview-about animatedParent animateOnce">
        <div class="container-fluid">
            <div class="about-list afterclear animated fadeInUp go">
                <ul>
                    <li>
                        <div class="icon-img">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-a1.png" alt="">
                        </div>
                        <div class="icon-title text-center">
                            <h4>Lorem</h4>
                            <p>We were founded lorem</p>
                        </div>
                    </li>
                    <li>
                        <div class="icon-img">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-a2.png" alt="">
                        </div>
                        <div class="icon-title text-center">
                            <h4>Lorem</h4>
                            <p>We`ve experienced lorem</p>
                        </div>
                    </li>
                    <li>
                        <div class="icon-img">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-a3.png" alt="">
                        </div>
                        <div class="icon-title text-center">
                            <h4>Lorem</h4>
                            <p>We simplify life lorem</p>
                        </div>
                    </li>
                    <li>
                        <div class="icon-img">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-a4.png" alt="">
                        </div>
                        <div class="icon-title text-center">
                            <h4>Lorem</h4>
                            <p>We play board lorem</p>
                        </div>
                    </li>
                    <li>
                        <div class="icon-img">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-a5.png" alt="">
                        </div>
                        <div class="icon-title text-center">
                            <h4>Lorem</h4>
                            <p>We count lorem</p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="section-content section-sub-banner section-careers bg-white animatedParent animateOnce">
        <div class="container-fluid">
            <div class="col-md-6 pull-right">
                <div class="screening-img animated fadeInDownShort">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/about-2.png" alt="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="section-title transform-y pl-3">
                    <h3 class="animated fadeInUpShort">Careers Lorem</h3>
                    <p class="animated fadeInUpShort delay-250">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu est eu ipsum faucibus dictum. Cras mattis ornare urna quis lacinia. Sed tristique eleifend maximus.
                    </p>
                    <p class="animated fadeInUpShort delay-250">
                        Maecenas ultrices justo in accumsan vestibulum. Duis quis justo arcu. Vivamus vel risus sit amet dolor aliquam luctus. Curabitur molestie blandit est a ultricies.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-title text-center">
                <h3 class="animated fadeInUpShort">Services Lorem</h3>
                <div class="animated fadeInUpShort delay-250">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu est eu ipsum faucibus dictum. Cras mattis</p>
                </div>
            </div>
            <div class="section-cols four-cols section-top features-container afterclear animated fadeInUp">
                <div class="col-md-4">
                    <div class="col-content">
                        <div class="col-img text-center">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-l1.png" alt="">
                        </div>
                        <div class="col-desc text-center">
                            <h4>SUPPORT LOREM</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-content">
                        <div class="col-img text-center">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-l2.png" alt="">
                        </div>
                        <div class="col-desc text-center">
                            <h4>SOLUTION LOREM</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-content">
                        <div class="col-img text-center">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-l3.png" alt="">
                        </div>
                        <div class="col-desc text-center">
                            <h4>QUALITY LOREM</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-content">
                        <div class="col-img text-center">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-l4.png" alt="">
                        </div>
                        <div class="col-desc text-center">
                            <h4>HELP LOREM</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-content">
                        <div class="col-img text-center">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-l5.png" alt="">
                        </div>
                        <div class="col-desc text-center">
                            <h4>RELATIONSHIP LOREM</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-content">
                        <div class="col-img text-center">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-l6.png" alt="">
                        </div>
                        <div class="col-desc text-center">
                            <h4>RESPONSIBILITY LOREM</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--    <div class="section-content bg-light-gray animatedParent animateOnce">-->
    <!--        <div class="container-fluid">-->
    <!--            <div class="section-title text-center">-->
    <!--                <h3 class="animated fadeInUpShort">--><?php //the_field('team_title') ;?><!--</h3>-->
    <!--                <div class="animated fadeInUpShort delay-250">-->
    <!--                    --><?php //the_field('team_content') ;?>
    <!--                </div>-->
    <!--            </div>-->
    <!--            <div class="team-holder section-top just-center afterclear animated fadeInUp">-->
    <!--                <div class="col-md-6 col-sm-6">-->
    <!--                    <div class="team-list">-->
    <!--                        <div class="team-img fl-left">-->
    <!--                            <img class="transform-50" src="--><?php //echo get_stylesheet_directory_uri(); ?><!--/images/team-img.png" alt="">-->
    <!--                        </div>-->
    <!--                        <div class="team-content blue">-->
    <!--                            <h5>Brian - CEO at Origin</h5>-->
    <!--                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu est eu ipsum faucibus dictum. Cras mattis ornare urna quis lacinia. Sed tristique eleifend maximus.</p>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--                <div class="col-md-6 col-sm-6">-->
    <!--                    <div class="team-list">-->
    <!--                        <div class="team-img fl-left">-->
    <!--                            <img class="transform-50" src="--><?php //echo get_stylesheet_directory_uri(); ?><!--/images/team-img.png" alt="">-->
    <!--                        </div>-->
    <!--                        <div class="team-content yellow">-->
    <!--                            <h5>Marc - President at Origin</h5>-->
    <!--                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu est eu ipsum faucibus dictum. Cras mattis ornare urna quis lacinia. Sed tristique eleifend maximus.</p>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--                <div class="col-md-6 col-sm-6">-->
    <!--                    <div class="team-list">-->
    <!--                        <div class="team-img fl-left">-->
    <!--                            <img class="transform-50" src="--><?php //echo get_stylesheet_directory_uri(); ?><!--/images/team-img.png" alt="">-->
    <!--                        </div>-->
    <!--                        <div class="team-content green">-->
    <!--                            <h5>Jay - Executive Vice President at Origin Diagnostics</h5>-->
    <!--                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu est eu ipsum faucibus dictum. Cras mattis ornare urna quis lacinia. Sed tristique eleifend maximus.</p>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--                <div class="col-md-6 col-sm-6">-->
    <!--                    <div class="team-list">-->
    <!--                        <div class="team-img fl-left">-->
    <!--                            <img class="transform-50" src="--><?php //echo get_stylesheet_directory_uri(); ?><!--/images/team-img.png" alt="">-->
    <!--                        </div>-->
    <!--                        <div class="team-content green">-->
    <!--                            <h5>Larry - Chief Operations Officer</h5>-->
    <!--                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu est eu ipsum faucibus dictum. Cras mattis ornare urna quis lacinia. Sed tristique eleifend maximus.</p>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--                <div class="col-md-6 col-sm-6">-->
    <!--                    <div class="team-list">-->
    <!--                        <div class="team-img fl-left">-->
    <!--                            <img class="transform-50" src="--><?php //echo get_stylesheet_directory_uri(); ?><!--/images/team-img.png" alt="">-->
    <!--                        </div>-->
    <!--                        <div class="team-content yellow">-->
    <!--                            <h5>Allyson Mazer - Vice President Human Resource</h5>-->
    <!--                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu est eu ipsum faucibus dictum. Cras mattis ornare urna quis lacinia. Sed tristique eleifend maximus.</p>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <div class="section-content bg-gray section-parent-origin animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-title text-center">
                <h3 class="animated fadeInUpShort">Recent News</h3>
                <div class="animated fadeInUpShort delay-250">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu est eu ipsum faucibus dictum. Cras mattis</p>
                </div>
            </div>
            <div class="payroll-solutions news-content section-top afterclear animated fadeInUp">
                <div class="col-md-4">
                    <div class="ps-list text-center">
                        <div class="ps-icon">
                            <div class="ps-img transform-50">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/news-1.png" alt="">
                            </div>
                        </div>
                        <div class="ps-details">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu est eu ipsum faucibus dictum. Cras mattis</p>
                        </div>
                        <div class="ps-sub-heading">
                            <p>Lorem ipsum dolor sit amet</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="ps-list text-center">
                        <div class="ps-icon">
                            <div class="ps-img transform-50">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/news-1.png" alt="">
                            </div>
                        </div>
                        <div class="ps-details">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu est eu ipsum faucibus dictum. Cras mattis</p>
                        </div>
                        <div class="ps-sub-heading">
                            <p>Lorem ipsum dolor sit amet</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="ps-list text-center">
                        <div class="ps-icon">
                            <div class="ps-img transform-50">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/news-1.png" alt="">
                            </div>
                        </div>
                        <div class="ps-details">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu est eu ipsum faucibus dictum. Cras mattis</p>
                        </div>
                        <div class="ps-sub-heading">
                            <p>Lorem ipsum dolor sit amet</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="gap-80"></div>
    </div>
    <?php
endwhile; else :
endif;
get_footer();