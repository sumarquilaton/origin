<?php
/**
 * Services Template
 *
 *
 */

get_header();

global $post;
$post_slug=$post->post_name;

$bg_img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );

if ( have_posts() ) : while ( have_posts() ) : the_post();
    ?>
    <div class="section-content bg-inline animatedParent animateOnce" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/bg-request.png')">
        <div class="container-fluid">
            <div class="section-title section-mw500 text-white text-center">
                <h2 class="animated fadeInUpShort"><?php the_field('services_title',49); ?></h2>
                <div class="animated fadeInUpShort delay-250"><p><?php the_field('services_content',49); ?></p></div>
                <div class="gap-30"></div>
                <div class="animated fadeInUpShort delay-250 go">
                    <a href="<?php echo site_url(); ?>/background-screening/#zservices" class="btn-common">Learn More</a>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-sub-banner bg-white animatedParent animateOnce">
        <div class="container-fluid">
            <div class="single-services">
                <div class="text-center">
                    <div class="screening-img animated fadeInDownShort">
                        <img src="http://d6webdevelopment.com/origin-home/wp-content/uploads/2018/07/graded.png" alt="">
                    </div>
                </div>
                <div class="gap-30"></div>
                <div class="section-title">
                    <h3 class="text-center animated fadeInUpShort"><?php the_title(); ?></h3>
                    <div class="animated fadeInUpShort delay-250">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-gap pt-0">
        <div class="gap-100"></div>
    </div>
    <div class="section-content animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-request bg-inline bg-request animated fadeInUp">
                <div class="section-title text-center text-white">
                    <h3><?php the_field('request_title',49); ?></h3>
                    <p><?php the_field('request_content',49); ?></p>
                    <div class="gap-30"></div>
                    <a href="#request" data-toggle="modal" class="btn-common"><?php the_field('request_button_text',49); ?></a>
                </div>
            </div>
        </div>
    </div>

    <?php
endwhile; else :
endif;
get_footer(); ?>