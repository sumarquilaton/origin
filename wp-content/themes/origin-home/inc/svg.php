<div class="workforce-wrap">

    <div class="workforce-svg-wrap">

        <div class="circles">
            <div class="circle circle1"></div>
            <div class="circle circle2"></div>
            <div class="circle circle3"></div>
            <div class="circle circle4"></div>
            <div class="circle circle5"></div>
            <div class="circle circle6"></div>
        </div>

        <div class="workforce">

            <div class="svg-wrap">
                <!-- <div class="desk"></div> -->
                <div class="workforce-svg">
                    <img src="<?= get_stylesheet_directory_uri()?>/images/designer2.jpg">
                </div>
            </div>
        </div>
    </div>


    <div class="workforce-items">
        <?php $cntr = 0; while (have_rows('workforce_platform',7)): the_row(); ?>
            <div class="workforce-item <?php echo ($cntr == 0) ? 'bottom-icon' : '' ?>">
                <div class="icon"><img src="<?php the_sub_field('workforce_icon'); ?>" alt=""></div>
                <h3><?php the_sub_field('workforce_title'); ?></h3>
                <p><?php the_sub_field('workforce_description'); ?></p>
            </div>
            <?php $cntr++; endwhile; ?>
    </div>
</div>