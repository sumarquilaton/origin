<svg version="1.1" id="Objects" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 800 330" style="enable-background:new 0 0 800 330;" xml:space="preserve">
<style type="text/css">
    .st52{fill:none;stroke:#1DB0D8;stroke-miterlimit:10;}
    .st53{fill:none;stroke:#DBDBDB;stroke-width:0.5;stroke-miterlimit:10;}
    .st54{fill:#F2F2F2;}
    .st55{fill:#6AC25A;}
    .st56{fill:#FDFEFD;}
    .st57{fill:#FAFCFA;}
</style>
    <circle class="st52" cx="298.1" cy="258.1" r="64.5"/>
    <circle class="st52" cx="298.5" cy="259.3" r="51"/>
    <rect x="95.7" y="33.1" class="st53" width="196" height="144.4"/>
    <rect x="104.7" y="53.7" class="st54" width="74.2" height="40"/>
    <rect x="104.7" y="101.4" class="st54" width="73.1" height="4.5"/>
    <rect x="102.1" y="123.3" class="st54" width="184.4" height="9"/>
    <rect x="102.1" y="137.5" class="st54" width="184.4" height="9"/>
    <rect x="102.1" y="153" class="st54" width="184.4" height="9"/>
    <rect x="195" y="65.3" class="st54" width="85.1" height="7.7"/>
    <rect x="195" y="53.7" class="st54" width="85.1" height="7.7"/>
    <rect x="195" y="76.9" class="st54" width="85.1" height="7.7"/>
    <rect x="195" y="88.5" class="st54" width="85.1" height="7.7"/>
    <rect x="195" y="100.1" class="st54" width="85.1" height="7.7"/>
    <line class="st53" x1="206.6" y1="177.5" x2="242.9" y2="223.3"/>
    <g>
        <path class="st55" d="M278.9,6.4c3.2,0,6.3,0,9.5,0c0.2,0.5,0.7,0.5,1.1,0.5c14.4,3.1,23.4,11.8,27.2,26.1c0.2,0.8-0.2,1.8,0.7,2.4
		c0,3.3,0,6.7,0,10c-0.9,0.7-0.8,1.9-1,2.9c-3.5,14.1-15.9,24.6-30.4,25.5c-17.9,1.2-32.9-11.2-35.8-29.4
		c-2.6-16.5,8.8-32.9,25.5-37C276.8,7.1,278,7.3,278.9,6.4z M277.5,57.3c0.6-0.6,1.2-1.1,1.8-1.7c7.3-7.3,14.7-14.7,22-22
		c3.4-3.4,3.3-3.3,0-6.8c-1.3-1.4-2-1.2-3.3,0.1c-6.2,6.3-12.5,12.5-18.7,18.9c-1.4,1.4-2.2,1.8-3.7,0.1c-1.5-1.7-3.2-3.3-4.8-4.9
		c-3.4-3.4-3.4-3.4-6.8,0.1c-1.1,1.1-1.4,1.7-0.1,2.9c4.1,4,8.1,8,12.1,12.1C276.5,56.5,277,56.8,277.5,57.3z"/>
        <path class="st56" d="M278.9,6.4c-0.9,0.9-2.1,0.7-3.1,1c-16.8,4.2-28.1,20.5-25.5,37c2.9,18.3,17.9,30.6,35.8,29.4
		c14.5-1,26.8-11.4,30.4-25.5c0.2-1,0.1-2.1,1-2.9c0,9.1,0,18.3,0,27.4c0,1.3-0.3,1.6-1.6,1.6c-21.6,0-43.2,0-64.8,0
		c-1.3,0-1.6-0.3-1.6-1.6c0-21.6,0-43.2,0-64.8c0-1.3,0.3-1.6,1.6-1.6C260.3,6.4,269.6,6.4,278.9,6.4z"/>
        <path class="st56" d="M317.4,35.4c-0.9-0.6-0.5-1.6-0.7-2.4c-3.7-14.3-12.7-23-27.2-26.1c-0.4-0.1-0.9,0-1.1-0.5
		c9.1,0,18.3,0,27.4,0c1.3,0,1.6,0.3,1.6,1.6C317.4,17.1,317.4,26.2,317.4,35.4z"/>
        <path class="st57" d="M277.5,57.3c-0.5-0.4-1-0.8-1.5-1.2c-4-4-8-8.1-12.1-12.1c-1.3-1.2-1-1.9,0.1-2.9c3.5-3.5,3.4-3.5,6.8-0.1
		c1.6,1.6,3.4,3.1,4.8,4.9c1.4,1.7,2.3,1.3,3.7-0.1c6.2-6.3,12.5-12.5,18.7-18.9c1.2-1.3,1.9-1.5,3.3-0.1c3.3,3.5,3.4,3.4,0,6.8
		c-7.3,7.3-14.7,14.7-22,22C278.8,56.1,278.2,56.7,277.5,57.3z"/>
    </g>
    <polygon class="st54" points="277.2,57.6 304.3,30.5 298.8,25 276.9,46.9 267.2,37.3 262.1,42.3 "/>
    <div class="screen-img animatedParent animateOnce">
        <div class="animated fadeInUpShort go">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/bg-screening.png" alt="">
        </div>
    </div>
</svg>