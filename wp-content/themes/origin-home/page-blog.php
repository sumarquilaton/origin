<?php
/**
 * Template Name: Blogs
 *
 *
 */

get_header();

global $post;
$post_slug=$post->post_name;

$bg_img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );

if ( have_posts() ) : while ( have_posts() ) : the_post();
?>
    <div class="section-content bg-inline animatedParent animateOnce" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/bg-request.png')">
        <div class="container-fluid">
            <div class="section-title section-mw500 text-white text-center">
                <h2 class="animated fadeInUpShort go"><?php the_field('heading'); ?></h2>
                <?php the_field('content'); ?>
            </div>
        </div>
    </div>
    <div class="section-content section-parent-origin animatedParent animateOnce">
        <div class="container-fluid">
            <div class="news-content blog-page">
                <?php
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                $myquery = new WP_Query(
                        array(
                                'post_type' => 'post',
                                'order'     => 'ASC',
                                'posts_per_page' => '9',
                                'paged'=>$paged
                            // add any other parameters to your wp_query array
                        )
                );

                if ($myquery->have_posts()) : ?>
                    <div class="blog-list afterclear">
                        <?php $i=0; while ($myquery->have_posts()) : $myquery->the_post(); ?>
                            <div class="col-md-4 col-sm-6">
                                <a href="<?php the_permalink(); ?>">
                                    <div class="ps-list scale-holder text-center">
                                        <div class="ps-icon">
                                            <div class="ps-img bg-inline" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));?>'); ">
                                            </div>
                                        </div>
                                        <div class="ps-details">
                                            <p>
                                                <?php
                                                $content = get_the_content();
                                                $content_filter = wp_filter_nohtml_kses( $content );
                                                echo mb_strimwidth($content_filter, 0, 150, '...')
                                                ?>
                                            </p>
                                        </div>
                                        <div class="ps-sub-heading">
                                            <?php global $post;
                                            $terms = wp_get_post_terms( $post->ID, 'category');
                                            ?>
                                            <p><?php echo $terms[0]->name; ?></p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php endwhile; ?>
                    </div>
                    <div class="blog-pagination afterclear text-center animated fadeInUp delay-500">
                        <?php wp_pagenavi( array( 'query' => $myquery ) ); ?>
                        <?php wp_reset_query(); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="section-content animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-request bg-inline bg-request animated fadeInUp">
                <div class="section-title text-center text-white">
                    <h3><?php the_field('request_title'); ?></h3>
                    <p><?php the_field('request_content'); ?></p>
                    <div class="gap-30"></div>
                    <a href="#request" data-toggle="modal" class="btn-common"><?php the_field('request_button_text'); ?></a>
                </div>
            </div>
        </div>
    </div>
<?php
    endwhile; else :
endif;
get_footer();