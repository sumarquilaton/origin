<?php
/**
 * Template Name: Home Page
 *
 *
 */

get_header();

global $post;
$post_slug=$post->post_name;

$bg_img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );

if ( have_posts() ) : while ( have_posts() ) : the_post();
?>
<div class="section-banner animatedParent animateOnce">
    <div class="banner-home bg-inline" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/banner-home.png');">
        <div class="container-fluid">
            <div class="banner-mobile">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mobile-home.png" alt="">
            </div>
            <div class="banner-content transform-y">
                <h2 class="animated fadeInUpShort go"><?php the_field('heading'); ?></h2>
                <?php the_field('content'); ?>
                <div class="gap-30"></div>
                <div class="animated fadeInUpShort delay-250 go">
                    <a href="#lightbox" data-target="#lightbox" data-toggle="modal" class="btn-common"><?php the_field('button_text'); ?></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-content animatedParent animateOnce">
    <div class="container-fluid">
        <div class="section-title text-center animated fadeInUpShort">
            <h3><?php the_field('workforce_title'); ?></h3>
        </div>
        <div class="text-center animated fadeInUpShort delay-250">
            <?php include_once "inc/svg.php"?>
        </div>
    </div>
</div>
<div class="section-content section-board bg-gray animatedParent animateOnce">
    <div class="container-fluid">
        <div class="col-md-8">
            <div class="board-img animated fadeInDownShort">
                <img src="<?php the_field('mobile_image'); ?>" alt="">
            </div>
        </div>
        <div class="col-md-4">
            <div class="section-title afterclear">
                <div class="board-content transform-y">
                    <h3><?php the_field('origin_title'); ?></h3>
                    <p><?php the_field('origin_content'); ?></p>
                    <div class="gap-5"></div>
                    <h4><?php the_field('workforce_mobile_title'); ?></h4>
                    <?php the_field('workforce_list'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-content animatedParent animateOnce">
    <div class="container-fluid">
        <div class="section-title text-center">
            <h3 class="animated fadeInUpShort"><?php the_field('works_for_you_title'); ?></h3>
            <p class="animated fadeInUpShort delay-250"><?php the_field('works_for_you_content'); ?></p>
        </div>
        <div class="section-cols section-top afterclear animated fadeInUp">
            <?php while (have_rows('works_for_you_list')): the_row(); ?>
                <div class="col-md-4 p-0">
                    <div class="col-content">
                        <div class="col-img text-center">
                            <img src="<?php the_sub_field('icon'); ?>" alt="">
                        </div>
                        <div class="col-desc text-center">
                            <h4><?php the_sub_field('works_title'); ?></h4>
                            <p><?php the_sub_field('works_description'); ?></p>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
</div>
<div class="section-content pt-0">
    <div class="container-fluid animatedParent animateOnce">
        <div class="section-title text-center">
            <h3 class="animated fadeInUpShort"><?php the_field('partners_title'); ?></h3>
            <p class="animated fadeInUpShort delay-250"><?php the_field('partners_content'); ?></p>
        </div>
        <div class="animated fadeInUpShort delay-250 text-center ">
            <div class="gap-30"></div>
            <a href="#partners" data-target="#partners" data-toggle="modal" class="btn-common"><?php the_field('button_text'); ?></a>
        </div>
        <div class="section-partners section-top animated fadeInUp">
            <ul class="text-center">
                <?php $cntr = 0; while (have_rows('partners_list')): the_row(); ?>
                    <li class="<?php echo ($cntr == 2) ? 'active' : '' ?>">
                        <img src="<?php the_sub_field('partners_image'); ?>" alt="">
                    </li>
                <?php $cntr++; endwhile; ?>
            </ul>
        </div>
    </div>
</div>
<div class="section-content section-parent-origin section-sub-banner section-time-off bg-gray animatedParent animateOnce">
    <div class="section-origin">
        <div class="container-fluid">
            <div class="col-md-6 pull-right">
                <div class="screening-img animated fadeInDownShort">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/create.png" alt="">
                </div>
                <div class="section-create-dashboard animated fadeInDownShort">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/create-dashboard.jpg" alt="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="section-title transform-y">
                    <div class="origin-desc">
                        <h3 class="animated fadeInDownShort"><?php the_field('why_did_title'); ?></h3>
                        <div class="animated fadeInDownShort delay-250">
                            <p>
                                <?php the_field('why_did_content'); ?>
                            </p>
                        </div>

                    </div>

                    <div class="testi-holder animated fadeInUpShort delay-500">
                        <p>
                            <?php the_field('testimonial_content'); ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-content animatedParent animateOnce">
    <div class="container-fluid">
        <div class="section-request bg-inline bg-request animated fadeInUp">
            <div class="section-title text-center text-white">
                <h3><?php the_field('request_title',7); ?></h3>
                <p><?php the_field('request_content',7); ?></p>
                <div class="gap-30"></div>
                <a href="#request" data-toggle="modal" class="btn-common"><?php the_field('request_button_text',7); ?></a>
            </div>
        </div>
    </div>
</div>
<div class="modal-gallery modal fade" tabindex="-1" id="lightbox" role="dialog">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <div class="modal-dialog">
        <div class="modal-content animated fadeInUpShort go">
            <div class="modal-body">
                <iframe width="50%" height="450" src="<?php the_field('video_link'); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal-gallery modal fade" tabindex="-1" id="partners" role="dialog">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body animated fadeInUpShort go">
                <iframe width="50%" height="450" src="<?php the_field('partners_video_link'); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php
    endwhile; else :
endif;
get_footer();
