<?php
/**
 * Template Name: HR Management
 *
 *
 */

get_header();

global $post;
$post_slug=$post->post_name;

$bg_img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );

if ( have_posts() ) : while ( have_posts() ) : the_post();
?>
    <div class="section-banner animatedParent animateOnce">
        <div class="banner-home bg-inline" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/hr-banner.png');">
            <div class="hr-banner-group banner-desktop">
                <div class="hr-banner hr-image animated fadeInUpShort go delay-250">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/hr-icon.png" alt="">
                </div>
                <div class="hr-banner hr-building animated fadeInDownShort go">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/hr-building.png" alt="">
                </div>
            </div>
            <div class="banner-mobile">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/hr-icon.png" alt="">
            </div>
            <div class="container-fluid">
                <div class="banner-content transform-y">
                    <h2 class="animated fadeInUpShort go"><?php the_field('heading'); ?></h2>
                    <?php the_field('content'); ?>
                    <div class="gap-30"></div>
                    <div class="animated fadeInUpShort delay-250 go">
                        <a href="#hrmanage" data-toggle="modal" class="btn-common"><?php the_field('button_text'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content pt-0 animatedParent animateOnce">
        <div class="container-fluid p-0">
            <div class="laptop-content-list hr-list mw-initial afterclear animated fadeInUp go">
                <?php while (have_rows('management_list')): the_row(); ?>
                <div class="col-md-2 col-sm-3 col-xs-6 p-0">
                    <div class="icon-list">
                        <div class="icon-img">
                            <img src="<?php the_sub_field('management_icon'); ?>" alt="">
                        </div>
                        <div class="icon-title">
                            <h4><?php the_sub_field('management_title'); ?></h4>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
    <div class="section-content section-sub-banner bg-gray animatedParent animateOnce">
        <div class="container-fluid">
            <div class="col-md-6 pull-right">
                <div class="screening-img animated fadeInDownShort">
                    <img src="<?php the_field('applicant_image'); ?>" alt="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="section-title transform-y">
                    <h3 class="animated fadeInUpShort"><?php the_field('applicant_title'); ?></h3>
                    <div class="animated fadeInUpShort delay-250">
                        <?php the_field('applicant_content'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-sub-banner bg-white animatedParent animateOnce">
        <div class="container-fluid">
            <div class="col-md-6">
                <div class="screening-img max-400 animated fadeInDownShort">
                    <img src="<?php the_field('employee_image'); ?>" alt="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="section-title transform-y">
                    <h3 class="animated fadeInUpShort"><?php the_field('employee_title'); ?></h3>
                    <div class="animated fadeInUpShort delay-250">
                        <?php the_field('employee_content'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content bg-gray animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-title text-center">
                <h3 class="animated fadeInUpShort"><?php the_field('performance_title'); ?></h3>
                <div class="animated fadeInUpShort delay-250">
                    <?php the_field('performance_content'); ?>
                </div>
            </div>
            <div class="section-cols four-cols section-top afterclear animated fadeInUp">
                <?php while (have_rows('performance_list')): the_row(); ?>
                    <div class="col-md-3">
                        <div class="col-content">
                            <div class="col-img text-center">
                                <img src="<?php the_sub_field('performance_icon'); ?>" alt="">
                            </div>
                            <div class="col-desc text-center">
                                <h4><?php the_sub_field('p_title'); ?></h4>
                                <p><?php the_sub_field('p_content'); ?></p>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
    <div class="section-content section-sub-banner bg-white animatedParent animateOnce">
        <div class="container-fluid">
            <div class="col-md-6 pull-right">
                <div class="screening-img max-400 animated fadeInDownShort">
                    <img src="<?php the_field('survey_image'); ?>" alt="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="section-title transform-y">
                    <h3 class="animated fadeInUpShort"><?php the_field('survey_title'); ?></h3>
                    <div class="animated fadeInUpShort delay-250">
                        <?php the_field('survey_content'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-sub-banner section-track bg-white pt-0 section-time-off animatedParent animateOnce">
        <div class="container-relative afterclear">
            <div class="col-md-7 p-0">
                <div class="screening-img animated fadeInDownShort">
                    <img src="<?php the_field('tracking_image'); ?>" alt="">
                </div>
            </div>
            <div class="col-md-5">
                <div class="section-title transform-y">
                    <h3 class="animated fadeInUpShort"><?php the_field('tracking_title'); ?></h3>
                    <div class="animated fadeInUpShort delay-250">
                        <?php the_field('tracking_content'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content bg-gray section-parent-origin animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-title text-center">
                <h3 class="animated fadeInUpShort"><?php the_field('portal_title'); ?></h3>
                <div class="animated fadeInUpShort delay-250">
                    <?php the_field('portal_content'); ?>
                </div>
            </div>
            <div class="gap-30"></div>
            <div class="section-laptop animated fadeInUp">
                <img src="<?php the_field('portal_image'); ?>" alt="">
            </div>
         </div>
    </div>
    <div class="section-content animatedParent animateOnce">
        <div class="container-fluid">
            <div class="section-request bg-inline bg-request animated fadeInUp">
                <div class="section-title text-center text-white">
                    <h3><?php the_field('request_title'); ?></h3>
                    <p><?php the_field('request_content'); ?></p>
                    <div class="gap-30"></div>
                    <a href="#request" data-toggle="modal" class="btn-common"><?php the_field('request_button_text'); ?></a>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-gallery modal fade" id="hrmanage">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body animated fadeInUpShort go">
                    <video width="50%" height="450" controls poster="<?php echo get_template_directory_uri(); ?>/images/poster-3.jpg">
                        <source src="<?php the_field('video_file'); ?>" type="video/mp4">
                    </video>
                </div><!-- /.modal-body -->
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php
    endwhile; else :
endif;
get_footer();